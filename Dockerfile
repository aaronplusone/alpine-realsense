# syntax=docker/dockerfile:latest

FROM alpine

COPY alpine-2_49_0.patch /alpine-2_49_0.patch

RUN apk add --no-cache git libusb-dev py3-pip libressl-dev gcc g++ \
    eudev-dev pkgconfig gtk+3.0 libx11-dev glfw-dev make cmake ncurses \
 && git clone https://github.com/IntelRealSense/librealsense.git -b v2.49.0 \
 && git -C librealsense apply /alpine-2_49_0.patch \
 && mkdir -p librealsense/build \
 && cd librealsense/build \
 && cmake ../ -DCMAKE_BUILD_TYPE=Release -DBUILD_GRAPHICAL_EXAMPLES=false \
 && make -j30 \
 && make install \
 && cd / \
 && rm -rf librealsense
